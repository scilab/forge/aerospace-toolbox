/* ==================================================================== */
/* Allan CORNET */
/* DIGITEO 2009 */
/* Template aerospace_toolbox */
/* This file is released into the public domain */
/* ==================================================================== */
#ifndef __CSUM_H__
#define __CSUM_H__

/**
* csum function
* @param[in] a
* @param[in] b
* @param[in,out] c result of a + b
* @return 0
*/
int csum(double *a, double *b, double *c);

#endif /* __CSUM_H__ */

