/* ==================================================================== */
/* Allan CORNET */
/* DIGITEO 2009 */
/* Template aerospace_toolbox */
/* This file is released into the public domain */
/* ==================================================================== */
#ifndef __CSUB_H__
#define __CSUB_H__

/**
* csub function
* @param[in] a
* @param[in] b
* @param[in,out] c result of a - b
* @return 0
*/
int csub(double *a, double *b, double *c);

#endif /* __CSUB_H__ */

