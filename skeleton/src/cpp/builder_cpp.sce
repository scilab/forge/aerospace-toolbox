//
// builder_cpp.sce
// Copyright (C) James Goppert 2010 <jgoppert@users.sourceforge.net>
//
// builder_cpp.sce is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// builder_cpp.sce is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

src_cpp_path = get_absolute_file_path('builder_cpp.sce');

CFLAGS = "-I" + src_cpp_path;

tbx_build_src(['cppHello'], ['hello.cpp'], 'cpp', ..
              src_cpp_path, '', '', CFLAGS);

clear tbx_build_src;
clear src_cpp_path;
clear CFLAGS;
