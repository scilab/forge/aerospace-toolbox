// ====================================================================
// Allan CORNET
// INRIA 2008
// Template aerospace_toolbox
// This file is released into the public domain
// ====================================================================
//
//
function s = scilab_sum(valA,valB)
  s = valA + valB;
endfunction
// ====================================================================
