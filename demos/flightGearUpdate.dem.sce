//
// flightGearUpdate.dem.sce
// Copyright (C) James Goppert 2010 <jgoppert@users.sourceforge.net>
//
// flightGearUpdate.dem.sce is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// flightGearUpdate.dem.sce is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.
//

// ====================================================================
mode(-1);
lines(0);

disp('Please start flightGear with the script in the aerospace-toolbox folder');
disp('./your-path/aerospace-toolbox/fgrun.sh'); 
disp('Warning: if flightGear is not started this program will freeze scilab.');
disp('type <resume> when flightGear has started.');
pause
disp('aileron, elevator, rudder are mapped from -1 to 1');
disp('throttle is mapped from 0 to 1');
disp('Example: ');
disp('aileron: .1, elevator: .2, rudder: .3, throttle: 1');
disp('y=flightGearUpdate([.1;.2;.3;1])')
y=flightGearUpdate([.1;.2;.3;1])
disp(y)

// ====================================================================
// vim:ts=4:sw=4
